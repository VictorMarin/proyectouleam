package victormarin.facci.proyectouleam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadSensores extends AppCompatActivity {
    Button botonAcelerometro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensores);

        botonAcelerometro = (Button) findViewById(R.id.btnSensores);
        botonAcelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadSensores.this,ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });
    }
}

